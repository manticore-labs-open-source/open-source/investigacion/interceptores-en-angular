# Interceptores en angular

Desde la versión 4.3 de Angular tenemos disponible un nuevo módulo para la gestión de las llamadas HTTP que realiza nuestro proyecto, HttpClientModule. Entre otras novedades, nos ofrece la opción de implementar interceptores.

Un interceptor es una función que nos permite modificar la respuesta o petición de las llamadas que realizamos. Por ejemplo, nos permite añadir cabeceras a todas las peticiones o capturar códigos de respuesta erróneos que recibamos por parte del servidor. Gracias a los interceptores, estas acciones solo tenemos que definirlas y configurarlas una vez, y no por cada llamada HTTP que realicemos, como era necesario en versiones anteriores de Angular.

Ejemplos de uso de interceptores
Se puede crear interceptores que se apliquen tanto para las respuestas del servidor como para las peticiones que realicemos a éste. Ejemplos de cada una de ellas son:

Añadir token de autenticación a las llamadas que realicemos al servidor.
Añadir cabeceras extras a las peticiones.
Almacenar un log de las peticiones realizadas por la aplicación.
Cambiar el formato de las respuestas recibidas del servidor.
Capturas errores en las respuestas (404, 500, …).
Debug de las respuestas recibidas.
Interceptores en acción


Comenzamos creando un nuevo proyecto y creamos el archivo correspondiente al interceptor de las peticiones, con el siguiente contenido:


![](./imagenes/Selección_163.png)


Como vemos, el interceptor se encarga de modificar la petición que realizamos al servidor, añadiendo el parámetro “_limit” a la URL.

Continuamos creando el archivo para el interceptor de las respuestas:

![](./imagenes/Selección_164.png)


Si el servidor nos devuelve un error del tipo 404, se mostrará en consola el texto “Página no encontrada!”.En el caso de que se produzca otro tipo de error, representamos en consola el código y texto del mismo.

Configurando interceptores en Angular
Una vez creados los interceptores, debemos añadirlos al archivo AppModule:

![](./imagenes/Selección_165.png)

Añadimos los interceptores al nodo “providers” de nuestro módulo. Además, importamos el módulo HttpClientModule para poder realizar llamadas HTTP y probar nuestros interceptores.

Para esto, modificamos el componente AppComponent de la siguiente forma:

![](./imagenes/Selección_166.png)

Por último, modificamos el archivo HTML del componente para representar el listado de posts:

![](./imagenes/Selección_167.png)


Para comprobar el funcionamiento de nuestros interceptores, hacemos uso del servicio JSONPlaceholder, que nos ofrece una API REST para pruebas. En el método ngOnInit, realizamos una llamada HTTP del tipo GET para obtener un listado de posts. Os dejo tres posibles variables para la URL, de forma que podáis observar los distintos tipos de errores representados en la consola por el interceptor.


Como podemos comprobar, una vez creados y configurados los interceptores en el módulo, no necesitamos hacer nada más con ellos cuando realizamos una llamada HTTP, por lo que resulta muy cómodo su uso. Además, nos ahorra tener que realizar la misma comprobación, transformación por cada llamada HTTP que realicemos.

![](./imagenes/Selección_168.png)

<a href="https://www.facebook.com/oscar.sambache" target="_blank"><img alt="Encuentrame en facebook:" height="35" width="35" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTANMlMcb_-pUSuzjnpvSynOA3Ontg9Z2I1NE24WQ_KAk34yYmh"/> Oscar Sambache</a>